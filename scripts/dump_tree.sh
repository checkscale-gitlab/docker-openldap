#!/bin/bash
source .env
docker-compose exec -e LDAPTLS_REQCERT=never openldap ldapsearch -x -Z -H ldap://openldap:1389 -b $LDAP_ROOT -D cn=system,$LDAP_ROOT -w $SYSUSER_PASSWORD
